# **lib_DFT-MQTT** <br/> _Stream of synchrophasors to MQTT broker_

This module reads the synchrophasors estimated by any DFT algorithm implemented in one of the available DSP modules and send them to a MQTT broker.

## Requirements
This module requires the [QMQTT](https://github.com/emqx/qmqtt) library to work.
```
cd ~/
git clone https://github.com/emqx/qmqtt
cd qmqtt
make
make install
```
