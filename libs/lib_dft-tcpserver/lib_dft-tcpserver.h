// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef LIB_DFT_TCPSERVER_H
#define LIB_DFT_TCPSERVER_H

#include <QtNetwork/QTcpSocket>
#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>

int  lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc);
void lib_destroy();
int  lib_process();
void lib_log(FILE *fid);

#endif // LIB_DFT_TCPSERVER_H
