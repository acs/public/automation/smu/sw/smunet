// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_dft-tcpserver.h"
#include "QObject"

static subCFG *smuCFG;
static subMEM *smuMEM;
static QTcpSocket **tcpSOC;

static dsp_phas_t *data;
static uint32_t Nr;

static  QString filename;
static  FILE *fp_log;
static  uint64_t log_count, file_duration;
static  bool     ch_state[smu_Nch]={0,0,0,0,0,0,0,0};

/// ---------- SMU: NET virtual methods section ----------

/**
 * @brief NET allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc)
{
    smuCFG = cfg;
    smuMEM = mem;
    tcpSOC = soc;

    Nr = smuCFG->dsp.rate;                              // Reporting rate
    data = (dsp_phas_t*)smuMEM->dsp_data;

    for (int i=0; i<smu_Nch; i++){
        if(smuCFG->dsp.state.at(i).contains("1", Qt::CaseInsensitive))
            ch_state[i]=true;
    }

    return 0;
}

/**
 * @brief NET destroy memory
 *
 */
void lib_destroy()
{
    fclose(fp_log);
}

/**
 * @brief NET process multi-channel sample code
 *
 */
int  lib_process()
{
    int nr = smuMEM->dsp_pos;
    data = (dsp_phas_t*)smuMEM->dsp_data;
    QString readings;
    if (!((*tcpSOC)==nullptr))
    {
        if ((*tcpSOC)->isOpen()){
            for (int i=0; i<smu_Nch; i++)
                if (smuCFG->dsp.state.at(i) == "1"){
                    readings.append(QString("\"reading%1\":[") .arg(i+1));
                    readings.append(QString("[\"channel\",\"%1\"],") .arg(smuCFG->dsp.alias.at(i)));
                    readings.append(QString("[\"magnitude\",%1],")   .arg(data[smuMEM->dsp_pos].A.ch[i]));
                    readings.append(QString("[\"phase\",%1],")       .arg(data[smuMEM->dsp_pos].P.ch[i]));
                    readings.append(QString("[\"frequency\",%1],")   .arg(data[smuMEM->dsp_pos].f.ch[i]));
                    readings.append(QString("[\"rocof\",%1]]")       .arg(data[smuMEM->dsp_pos].df.ch[i]));
                    if (i<7)
                        readings.append(QString(",\n"));
                    else
                        readings.append(QString("\n"));
                }
            QString tcp_msg = QString("{\"device\":\"%1\",\n\"timestamp\":\"%2\",\n%3}")
                            .arg(smuCFG->net.devID)
                            .arg(QString::number(data[nr].t_stamp))
                            .arg(readings);
            QTextStream T(*tcpSOC);
            T << tcp_msg;
            (*tcpSOC)->flush();
        }
    }
    else
        {
            if (smuCFG->datalog.datalog_mode==1)
                lib_log(fp_log);
        }
    if (smuCFG->datalog.datalog_mode==2)
        lib_log(fp_log);
    return 0;
    //return -1;
}

/**
 * @brief NET log output data
 *
 */
void lib_log(FILE *fid)
{
    QString route;
    if (smuCFG->datalog.duration>0)
        file_duration=smuCFG->dsp.rate*3600*smuCFG->datalog.duration;
    else
        file_duration=smuCFG->dsp.rate*3600;
    //FILE INITIALIZE
    if (log_count==0){
        filename = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss").append(".txt");
        if ((!smuCFG->datalog.route.isEmpty())&&(!smuCFG->datalog.route.endsWith(QChar('/'))))         
                smuCFG->datalog.route.append("/");
        route = smuCFG->datalog.route;
        route.append(filename);
        fp_log = fopen (route.toStdString().c_str(), "w+");
        if (fp_log==NULL)
            fp_log = fopen (filename.toStdString().c_str(), "w+");
        //FILE CONTENT CREATION
        QString content;
        content.append(QString("timestamp"));
        for (int i=0; i<smu_Nch; i++){
            if (ch_state[i]){
                for (int j=0;j<4;j++){
                    content.append(QString(",\"%1").arg(smuCFG->dsp.alias.at(i)));
                    switch(j) {
                        case 0:
                            content.append("_rms\"");
                            break;
                        case 1:
                            content.append("_phase(rad)\"");
                            break;
                        case 2:
                            content.append("_frequency\"");
                            break;
                        default:
                            content.append("_ROCOF\"");
                    }
                }
            }
        }
        fprintf(fp_log, content.toStdString().c_str());
    }

    //FILE WRITE
    
    fprintf(fp_log, "\n%10lld",data[smuMEM->dsp_pos].t_stamp);
    for (int i=0; i<smu_Nch; i++){
        if (ch_state[i]){              
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].A.ch[i]);
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].P.ch[i]);
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].f.ch[i]);
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].df.ch[i]);
        }
    }

    //INITIALIZE A NEW FILE
    if (log_count<file_duration)
        log_count++;
    else
        log_count=0;
    //

    return;
}
