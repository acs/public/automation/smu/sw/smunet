// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_raw-tcpserver.h"
#include "QObject"

static subCFG *smuCFG;
static subMEM *smuMEM;
static QTcpSocket **tcpSOC;

static uint32_t Nr,Tw;

static QStringList args;

static smu_mcsr_t *data;
static quint32 starting_sample;
static quint32 last_sample;
static qint64 t_stamp;

static  QString filename;
static  FILE *fp_log;
static  uint64_t log_count, file_duration;
static  bool    ch_state[smu_Nch]={0,0,0,0,0,0,0,0};

/// ---------- SMU: NET virtual methods section ----------

/**
 * @brief NET allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc)
{
    smuCFG = cfg;
    smuMEM = mem;
    tcpSOC = soc;

    
    if (smuCFG->net.ohost.isEmpty())
        return -2; // Invalid host
    if (!smuCFG->net.oport)
        return -3; // Invalid port
    
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Tw = 1000/Nr;                                       // Time window per frame (ms)
    data = (smu_mcsr_t*)smuMEM->dsp_data;
    
    for (int i=0; i<smu_Nch; i++){
        if(smuCFG->dsp.state.at(i).contains("1", Qt::CaseInsensitive))
            ch_state[i]=true;
    }

    return 0;
}

/**
 * @brief NET destroy memory
 *
 */
void lib_destroy()
{
    fclose(fp_log);
}

/**
 * @brief NET process multi-channel sample code
 *
 */
int  lib_process()
{
    int nr = smuMEM->dsp_pos;
    data = (smu_mcsr_t*)smuMEM->dsp_data;
    starting_sample = (smuMEM->dsp_pos)*(smuMEM->dsp_dim);
    last_sample = starting_sample+smuMEM->dsp_dim;
    QString readings;
    if (smuCFG->daq.mode == 1)
        t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr)*Tw).toMSecsSinceEpoch(); //timestamp
    else
        t_stamp = (QDateTime::currentMSecsSinceEpoch());
    if (!((*tcpSOC)==nullptr))
    {
        if ((*tcpSOC)->isOpen()){
        for (int i=0; i<smu_Nch; i++)
        {
            if (smuCFG->dsp.state.at(i) == "1")
            {
                readings.append(QString("\n\"ch\": %1,\n\"magnitude\":") 
                .arg(smuCFG->dsp.alias.at(i)));
                for (quint32 j=starting_sample; j<last_sample; j++)
                    readings.append(QString("%1,").arg(data[j].ch[i]));
            }
        }
        QString payload = QString("{\n\"device\": %1,\n\"timestamp\": %2,%3\n}")
                        .arg(smuCFG->net.devID)
                        .arg(QDateTime::fromMSecsSinceEpoch(t_stamp).toString("yyyy-MM-ddThh:mm:ss.zzz"))
                        .arg(readings);
        QTextStream T(*tcpSOC);
        T << readings;
        (*tcpSOC)->flush();
        }

    }
    else
        {
            if (smuCFG->datalog.datalog_mode==1)
                lib_log(fp_log);
        }
    if (smuCFG->datalog.datalog_mode==2)
        lib_log(fp_log);
    return 0;
}
/**
 * @brief NET log output data
 *
 */
void lib_log(FILE *fid)
{
    QString route;
    if (smuCFG->datalog.duration>0)
        file_duration=smuCFG->daq.rate*3600000*smuCFG->datalog.duration;
    else
        file_duration=smuCFG->daq.rate*3600000;
    //FILE INITIALIZE
    if (log_count==0){
        filename = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss").append(".txt");
        if ((!smuCFG->datalog.route.isEmpty())&&(!smuCFG->datalog.route.endsWith(QChar('/'))))
                smuCFG->datalog.route.append("/");
        route = smuCFG->datalog.route;
        route.append(filename);
        fp_log = fopen (route.toStdString().c_str(), "w+");
        if (fp_log==NULL)
            fp_log = fopen (filename.toStdString().c_str(), "w+");
        //FILE CONTENT CREATION
        QString content;
        content.append(QString("timestamp,"));
        for (int i=0; i<smu_Nch; i++){
            if (ch_state[i]){
                content.append(QString("\"%1\"") .arg(smuCFG->dsp.alias.at(i)));
                if(i<smu_Nch-1)
                    content.append(",");
            }
        }
        fprintf(fp_log, content.toStdString().c_str());
    }

    //FILE WRITE
    for (quint32 j=starting_sample; j<last_sample; j++){
        if (j==starting_sample)
            fprintf(fp_log, "\n%10lld",t_stamp);
        else
            fprintf(fp_log, "\n");
        for (int i=0; i<smu_Nch; i++){
            if (ch_state[i])
                fprintf(fp_log, ",%10.7f",data[j].ch[i]);
        }
    }

    //INITIALIZE A NEW FILE
    if (log_count<file_duration)
        log_count++;
    else
        log_count=0;
    //

    return;
}