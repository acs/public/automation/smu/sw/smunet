# SPDX-FileCopyrightText: 2017-2025 Cesar Cazal <carlo.guarnieri@ieee.org>
# SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
#
# SPDX-License-Identifier: Apache-2.0

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT += qmqtt

HEADERS += $$PWD/lib_raw-tcpserver.h
SOURCES += $$PWD/lib_raw-tcpserver.cpp
