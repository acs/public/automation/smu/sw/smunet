// SPDX-FileCopyrightText: 2017-2025 Cesar Cazal <cesar.cazal@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef LIB_RAWTCPSERVER_H
#define LIB_RAWTCPSERVER_H

#include <QtNetwork/QTcpSocket>
#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>

int  lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc);
void lib_destroy();
int  lib_process();
void lib_log(FILE *fid);

#endif // LIB_RAWTCPSERVER_H
