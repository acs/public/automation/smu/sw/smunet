// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_dc-mqtt.h"
#include "QObject"

static subCFG *smuCFG;
static subMEM *smuMEM;
static QTcpSocket **tcpSOC;
static Qmqtt *mqtt_client;

static syn_meas_t *data;
static uint32_t Nr;

static QString mqtt_user, mqtt_pass, mqtt_pub, mqtt_sub;
static QStringList args;

static  QString filename;
static  FILE *fp_log;
static  uint64_t log_count, file_duration;
static  bool    ch_state[smu_Nch]={0,0,0,0,0,0,0,0};

/// ---------- SMU: NET virtual methods section ----------

/**
 * @brief NET allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc)
{
    smuCFG = cfg;
    smuMEM = mem;
    tcpSOC = soc;

    // Scan additiona arguments
    args = smuCFG->net.oargs.split(",");
    mqtt_user = args.at(0).split("=").last();
    mqtt_pass = args.at(1).split("=").last();
    mqtt_pub  = args.at(2).split("=").last();
    mqtt_sub  = args.at(3).split("=").last();
    if (smuCFG->net.ohost.isEmpty())
        return -2; // Invalid host
    if (!smuCFG->net.oport)
        return -3; // Invalid port
    if (mqtt_user.isEmpty() != mqtt_pass.isEmpty())
        return -5; // Invalid credentials

    qDebug().noquote() << "libNET:" << " MQTT parameters {"
                       << QString("user:%1, pass:%2, pub:%3, sub:%4")
                          .arg(mqtt_user,mqtt_pass,mqtt_pub,mqtt_sub)
                       << "}";

    // Init MQTT client
    mqtt_client = new Qmqtt(QHostAddress(smuCFG->net.ohost),smuCFG->net.oport);

    if (!mqtt_user.isEmpty() && !mqtt_pass.isEmpty()){
        mqtt_client->setUsername(mqtt_user);
        mqtt_client->setPassword(mqtt_pass.toUtf8());
    }
    mqtt_client->connectToHost();

    Nr = smuCFG->dsp.rate;                              // Reporting rate
    data = (syn_meas_t*)smuMEM->dsp_data;

    for (int i=0; i<smu_Nch; i++){
        if(smuCFG->dsp.state.at(i).contains("1", Qt::CaseInsensitive))
            ch_state[i]=true;
    }

    return 0;
}

/**
 * @brief NET destroy memory
 *
 */
void lib_destroy()
{
    fclose(fp_log);
}

/**
 * @brief NET process multi-channel sample code
 *
 */
int  lib_process()
{
    int nr = smuMEM->dsp_pos;
    data = (syn_meas_t*)smuMEM->dsp_data;

    QString readings;
    for (int i=0; i<smu_Nch; i++)
        if (smuCFG->dsp.state.at(i) == "1"){
            readings.append(QString("\t\t{\n"));
            readings.append(QString("\t\t\t\"channel\": \"%1\",\n") .arg(smuCFG->dsp.alias.at(i)));
            readings.append(QString("\t\t\t\"rms\": %1,\n")   .arg(data[smuMEM->dsp_pos].rms.ch[i]));
            readings.append(QString("\t\t\t\"dc\": %1,\n")       .arg(data[smuMEM->dsp_pos].dc.ch[i]));
            readings.append(QString("\t\t\t\"ac\": %1,\n")   .arg(data[smuMEM->dsp_pos].ac.ch[i]));
            readings.append(QString("\t\t},\n"));
        }

    QString payload = QString("{\n\t\"device\": \"%1\",\n\t\"timestamp\": \"%2\",\n\t\"readings\": [\n%3\n\t]\n}")
                      .arg(smuCFG->net.devID)
                      .arg(QDateTime::fromMSecsSinceEpoch(data[nr].t_stamp).toString("yyyy-MM-ddThh:mm:ss.zzz"))
                      .arg(readings);
    QMQTT::Message message(smuMEM->dsp_pos, mqtt_pub, payload.toUtf8());

    if (smuCFG->datalog.datalog_mode==2)
        lib_log(fp_log);

    if (mqtt_client->isConnectedToHost())
        return (mqtt_client->publish(message));
    else
        {
            if (smuCFG->datalog.datalog_mode==1)
                lib_log(fp_log);
        }
    return -1;
}

/**
 * @brief NET log output data
 *
 */
void lib_log(FILE *fid)
{
    QString route;
    if (smuCFG->datalog.duration>0)
        file_duration=smuCFG->dsp.rate*3600*smuCFG->datalog.duration;
    else
        file_duration=smuCFG->dsp.rate*3600;
    //FILE INITIALIZE
    if (log_count==0){
        filename = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss").append(".txt");
        if ((!smuCFG->datalog.route.isEmpty())&&(!smuCFG->datalog.route.endsWith(QChar('/'))))
                smuCFG->datalog.route.append("/");
        route = smuCFG->datalog.route;
        route.append(filename);
        fp_log = fopen (route.toStdString().c_str(), "w+");
        if (fp_log==NULL)
            fp_log = fopen (filename.toStdString().c_str(), "w+");
        //FILE CONTENT CREATION
        QString content;
        content.append(QString("timestamp,"));
        for (int i=0; i<smu_Nch; i++){
            if (ch_state[i]){
                for (int j=0;j<3;j++){
                    content.append(QString(",\"%1").arg(smuCFG->dsp.alias.at(i)));
                    switch(j) {
                        case 0:
                            content.append("_rms\"");
                            break;
                        case 1:
                            content.append("_dc\"");
                            break;
                        default:
                            content.append("_ac\"");
                    }
                }
            }
        }
        fprintf(fp_log, content.toStdString().c_str());
    }

    //FILE WRITE
    
    fprintf(fp_log, "\n%10lld",data[smuMEM->dsp_pos].t_stamp);
    for (int i=0; i<smu_Nch; i++){
        if (ch_state[i]){
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].rms.ch[i]);
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].dc.ch[i]);
            fprintf(fp_log, ",%10.7f",data[smuMEM->dsp_pos].ac.ch[i]);
        }
    }
    qDebug()<<"dsp_pos"<<smuMEM->dsp_pos;

    //INITIALIZE A NEW FILE
    if (log_count<file_duration)
        log_count++;
    else
        log_count=0;
    //

    return;
}

Qmqtt::Qmqtt(const QHostAddress& host, const quint16 port, QObject* parent)
    : QMQTT::Client(host, port, parent)
{
    connect(this, SIGNAL(connected()),    this, SLOT(onConnected()));
    connect(this, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(this, SIGNAL(subscribed(QString,quint8)), this, SLOT(onSubscribed(QString)));
    connect(this, SIGNAL(received(QMQTT::Message)),   this, SLOT(onReceived(QMQTT::Message)));
}

Qmqtt::~Qmqtt()
{

}

void Qmqtt::onConnected()
{
    qDebug() << "libNET:" << " Connected to MQTT broker " << smuCFG->net.ohost;
    if (!mqtt_sub.isEmpty())
        mqtt_client->subscribe(mqtt_sub, 0);
}

void Qmqtt::onDisconnected()
{

}

void Qmqtt::onSubscribed(const QString& topic)
{
    qDebug() << "libNET:" << " Subscribed topic: " << topic;
}

void Qmqtt::onReceived(const QMQTT::Message& message)
{
    qDebug() << "libNET:" << " Publish received: " << QString::fromUtf8(message.payload());
}
