# SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
# SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
#
# SPDX-License-Identifier: Apache-2.0

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT +=

HEADERS += $$PWD/lib_template.h
SOURCES += $$PWD/lib_template.cpp
