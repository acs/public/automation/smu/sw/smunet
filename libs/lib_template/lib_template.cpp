// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_template.h"
#include "QObject"

static subCFG *smuCFG;
static subMEM *smuMEM;
static QTcpSocket **tcpSOC;

static smu_mcsc_t *data;
static uint32_t Nr;

static char  *data_addr;
static qint64 data_size, data_sent;

/// ---------- SMU: NET virtual methods section ----------

/**
 * @brief NET allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc)
{
    smuCFG = cfg;
    smuMEM = mem;
    tcpSOC = soc;

    if (!smuCFG->net.ohost.isEmpty()){
        tcpSOC = 0;
        if (!smuCFG->net.oport)
            return -3; // Invalid port
        if (smuCFG->net.oprot > AnyIP)
            return -4; // Invalid protocol

        *tcpSOC = new QTcpSocket();
        (*tcpSOC)->connectToHost(smuCFG->net.ohost,
                                 smuCFG->net.oport,
                                 QIODevice::WriteOnly,
                                 (QAbstractSocket::NetworkLayerProtocol)smuCFG->net.oprot);
        if (!(*tcpSOC)->isOpen())
            return -1; // Unable to reach host
        QObject::connect(*tcpSOC, SIGNAL(disconnected()),*tcpSOC, SLOT(deleteLater()));
    }

    Nr = smuCFG->dsp.rate;                              // Reporting rate
    data = (smu_mcsc_t*)smuMEM->dsp_data;

    return 0;
}

/**
 * @brief NET destroy memory
 *
 */
void lib_destroy()
{
    if (!smuCFG->net.ohost.isEmpty())
        (*tcpSOC)->disconnectFromHost();
}

/**
 * @brief NET process multi-channel sample code
 *
 */
int  lib_process()
{
    data = (smu_mcsc_t*)smuMEM->dsp_data;
    data_sent = 0;
    data_addr = (char*)&smuMEM->dsp_data[smuMEM->dsp_pos*smuMEM->dsp_inc];
//    data_addr = (char*)&data[smuMEM->dsp_pos*smuMEM->dsp_inc];
    data_size = smuMEM->dsp_inc;

    // TODO format data...

    if ((*tcpSOC)->isOpen()){
        data_sent = (*tcpSOC)->write(data_addr, data_size);
        (*tcpSOC)->flush();
    }

    return data_sent;
}

/**
 * @brief NET log output data
 *
 */
void lib_log(FILE *fid)
{

}
