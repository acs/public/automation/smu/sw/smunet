// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef LIB_RAWMQTT_H
#define LIB_RAWMQTT_H

#include <QtNetwork/QTcpSocket>
#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>
#include <qmqtt.h>

int  lib_allocate(subCFG *cfg, subMEM *mem, QTcpSocket **soc);
void lib_destroy();
int  lib_process();
void lib_log(FILE *fid);


class Qmqtt : public QMQTT::Client
{
    Q_OBJECT
public:
    Qmqtt(const QHostAddress& host, const quint16 port, QObject* parent = NULL);
    ~Qmqtt();

public slots:
    void onConnected();
    void onDisconnected();
    void onSubscribed(const QString& topic);
    void onReceived(const QMQTT::Message& message);
};

#endif // LIB_RAWMQTT_H
