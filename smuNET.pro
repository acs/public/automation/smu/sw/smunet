# SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
# SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
#
# SPDX-License-Identifier: Apache-2.0

QT = core network

#VERSION = "dft-tcpserver"
#VERSION = "dft-tcpclient"
#VERSION = "dft-mqtt"
#VERSION = "dc-mqtt"
#VERSION = "dc-tcpserver"
#VERSION = "raw-mqtt"
VERSION = "raw-tcpserver"

TARGET = net_$${VERSION}
DEFINES += NET_LIB=\"\\\"libs/lib_$${VERSION}/lib_$${VERSION}.h\\\"\" \
           NET_NAME=\"\\\"net_$${VERSION}\\\"\"

TEMPLATE = lib

DEFINES += SMUNET_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    smunet.cpp
HEADERS += \
    smunet.h \
    smunet_global.h

include(../smuLIB/smuLIB.pri)
include(libs/lib_$${VERSION}/lib_$${VERSION}.pri)
message("Building shared lib: net_$${VERSION}.so")

unix {
    target.path = /usr/lib/smu
    INSTALLS += target
}

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_LFLAGS_RELEASE -= -O1


BUILDDIR = ./build
DESTDIR = ../build/libs
OBJECTS_DIR = $$BUILDDIR
MOC_DIR = $$BUILDDIR
RCC_DIR = $$BUILDDIR
