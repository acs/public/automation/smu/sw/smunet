// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "smunet.h"
#include NET_LIB

static subCFG *smuCFG;
static subMEM *smuMEM;
static QTcpSocket **tcpSOC;

int smuNET_init(subCFG *cfg, subMEM *mem, QTcpSocket **soc)
{
    smuCFG = cfg;
    smuMEM = mem;
    tcpSOC = soc;

    // Allocate libray memory
    return(lib_allocate(cfg,mem,soc));
}

void smuNET_exit()
{
    // Destroy libray memory
    lib_destroy();
}

int smuNET_proc()
{
    // Processing method
    return (lib_process());
}

int smuNET_test()
{
    // Test method
    return 0;
}
