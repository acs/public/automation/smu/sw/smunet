# **smuNET** <br/> _Dynamic libraries for networking and communications_

smuNET is a framework for developing networking and communications plugins for [**smuSVC**](smusvc/README.md) daemon in the form of dynamic libraries.

## Architecture
The library exports four methods that are dynamically linked by the daemon smuSVC at run-time.The two methods _init()_ and _exit()_ are called when the library is loaded and unloaded, respectively. They are used to allocate/initialize/destroy the variables used in the library. The method _proc()_ forwards the processed data in a given format to the final host over the desired network interface. It is linked by a signal-slot mechanism to the smuDSP library, which emits a data-ready signal when a new batch of processed data is available in the buffer. The method _test()_ can be used to provide self-test capabilties to the library.

```mermaid
stateDiagram
    direction LR
    [*] --> _init()
    _init() --> _proc()
    _proc() --> _exit()
    _exit() --> [*]
    state _proc() {
      direction LR
      state ready <<choice>>
      _test() --> ready
      smuDSP --> ready : data_ready
      ready --> Host
    }
```

## Download
Create the folder **smuNET** inside the project folder **smu**, then clone the repository inside.
```
mkdir -p ~/smu
cd ~/smu
mkdir smuNET
git clone <repoNET> smuNET
```

Repeat the same procedure for the common library component **smuLIB** (_ignore if already done_).
```
cd ~/smu
mkdir smuLIB
git clone <repoLIB> smuLIB
```

## Development
To create a new library, create first a new library folder **lib\__newlib_** inside **libs**, where _newlib_ is the new library name (_use no space or symbols_).
```
cd ~/smu/smuNET/libs
mkdir lib_newlib
```

Copy **lib_template** content to the new folder.
```
cp lib_template/* lib_newlib/
```

Replace every instance of _template_ with _newlib_.
```
cd lib_newlib
sed -i 's/template/newlib/g;s/TEMPLATE/NEWLIB/g' *
rename 's/template/newlib/g' *
```

Develop the new library following the guidelines described in [lib_template](libs/lib_template/README.md).

## Build
Edit the project file **smuNET.pro**.
```
cd ~/smu/smuNET
nano smuNET.pro
```

Select the build target by replacing _template_ with _newlib_.
- VERSION = [- "template" -]
- VERSION = [+ "newlib" +]

Generate the Makefile and build the library.
```
qmake
make -j4
```

Upon succesful compilation, the built library will be available in the project **build/libs** folder with the name **libnet\__newlib_.so**.
```
cd ~/smu/build/libs/
ls -l
```

## Usage
To use the newly built dynamic library at service startup, edit the SMU config file.
```
cd ~/smu/build
nano smuSVG.cfg
```

Change the parameters of the section **# DSP module** as needed, specifying the listening port *iport* on localhost, the message encoding *iappl*, the target host *ohost* and port *oport*, the NET processing library *oappl* to be used (net\__\<newlib\>_) and eventual parameters *oargs* to be passed as arguments.
```
# NET module
ihost:
iport:7080
iprot:0
itran:0
iappl:JSON
ohost:134.130.169.27
oport:1883
oprot:0
otran:0
oappl:net_newlib
oargs:args
```

To change the NET library at run-time, the graphical interface smuGUI offers an easy configuration tool. Alternatively, it is possible to change the configuration by sending a simple text message to the SMU host on port 7080. The message payload is given by the previous configuration lines separated by **;** and enclosed in curly brakets **{ }**.
```
echo "{iport:7080;ohost:134...}" | nc host 7080
```
Not all the configuration entries need to be specified in the message, only those to be changed, for instance the NET processing module.
```
echo "{oappl:net_newlib}" | nc host 7080
```

## Examples libraries
- [**lib_template**](libs/lib_template/README.md): template for networking and communications
- [**lib_raw**](libs/lib_raw/README.md): direct stream of raw sample buffer to TCP client
- [**lib_dft-mqtt**](libs/lib_dft-mqtt/README.md): stream of phasors to MQTT broker

## Copyright
2017-2021, Carlo Guarnieri (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE.md"><img alt="Apache License v2.0" style="border-width:0" src="docs/apache-2_logo.png" width="88"/></a><br />This work is licensed under the <a rel="license" href="LICENSE.md">Apache License v2.0</a>.

## Funding
<a rel="funding" href="https://www.kopernikus-projekte.de/en/projects/ensure"><img alt="ENSURE" style="border-width:0" src="docs/ensure_logo.png" height="63"/></a>&nbsp;
<a rel="funding" href="https://www.bmbf.de/en/index.html"><img alt="BMBF" style="border-width:0" src="docs/bmbf_logo.png" height="63"/></a><br />
This work was supported by <a rel="ENSURE" href="https://www.kopernikus-projekte.de/en/projects/ensure">New Energy Network Structures for the Energy Transition</a> (ENSURE), a project funded by the German Federal Ministry of Education and Research (BMBF) Kopernicus Project programme under grant agreement No. 100297733.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Carlo Guarnieri Calò Carducci, Ph.D.](mailto:carlo.guarnieri@ieee.org)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)