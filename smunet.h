// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SMUNET_H
#define SMUNET_H

#include "smunet_global.h"
#include <QtNetwork/QTcpSocket>
#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>

extern "C" SMUNETSHARED_EXPORT int  smuNET_init(subCFG *cfg, subMEM *mem, QTcpSocket **soc);
extern "C" SMUNETSHARED_EXPORT void smuNET_exit();

extern "C" SMUNETSHARED_EXPORT int  smuNET_proc();
extern "C" SMUNETSHARED_EXPORT int  smuNET_test();

#endif // SMUNET_H
