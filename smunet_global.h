// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SMUNET_GLOBAL_H
#define SMUNET_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SMUNET_LIBRARY)
#  define SMUNETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SMUNETSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SMUNET_GLOBAL_H
